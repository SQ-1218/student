<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>学员信息管理系统</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  </head>
  
  <body>
  <h2>学员信息列表</h2>
  <form method="post" action="student/index">
 	 <input value="查 询" type="submit" id="searchbutton">
   </form> 
    <table border="1" id="studentList">
    <tr>
    	<th><a id="addStudent" href="student/addStudent">添加学员</a><th>
    </tr>
    	<tr>
    		<th>编号</th>
    		<th>姓名</th>
    		<th>性别</th>
    		<th>年龄</th>
    		<th>电话</th>
    		<th>email</th>
    		<th>班级</th>
    	</tr>
    	
    	<!--此处显示学生信息列表-->
        <c:forEach var="student" items="${studentList}">
           	 <tr>
				<td>${student.id}</td>
	    		<td>${student.name}</td>
	    		<td>${student.gender}</td>
	    		<td>${student.age}</td>
	    		<td>${student.telephone}</td>
	    		<td>${student.email}</td>
	    		<td>${student.className.cname}</td>     
              </tr>
		</c:forEach>
    </table>

   <script type="text/javascript" src="statics/js/jquery-1.8.3.min.js"></script>
   <script type="text/javascript" src="statics/js/index.js"></script>
   
  </body>
</html>
