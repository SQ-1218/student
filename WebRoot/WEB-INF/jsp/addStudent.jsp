<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>学员信息添加</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <form method="post" action="student/addStudent">
      <table>
    		<caption><h2>学员信息添加</h2></caption>
    		<tr>
    			<td>姓名</td>
    			<td><input type="text" name="name" id="name"/></td>
    		</tr>
    		<tr>
    			<td>年龄</td>
    			<td><input type="text" name="age" id="age"/></td>
    		</tr>
    		<tr>
    			<td>性别</td>
    			<td><input type="text" name="gender" id="gender"/></td>
    		</tr>
    		<tr>
    			<td>电话</td>
    			<td><input type="text" name="telephone" id="telephone"/></td>
    		</tr>
    		<tr>
    			<td>email</td>
    			<td><input type="text" name="email" id="email"/></td>
    		</tr>
    		<tr>
    			<td>班级</td>
	    		<td>
	    		<select name="classId" id="classId">
	    			<option value='0'>请选择</option>
	    			
			    </select>
			    </td>
    		</tr>
    		<tr>
    			<td><input type="submit" name="submit" id="submit" value="提交"/></td>
    		</tr>
    	</table>
    </form>
   <script type="text/javascript" src="statics/js/jquery-1.8.3.min.js"></script>
   <script type="text/javascript" src="statics/js/add.js"></script>

  </body>
</html>
