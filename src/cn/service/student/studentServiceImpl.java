package cn.service.student;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.dao.student.StudentMapper;
import cn.entity.Student;

@Transactional
@Service("studentService")
public class studentServiceImpl implements studentService{
	@Autowired
	private StudentMapper studentMapper;

	//查询所有的学员信息
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Student> getList() {
		// TODO Auto-generated method stub
		return studentMapper.getList();
	}

	//增加学员信息
	@Transactional(propagation=Propagation.REQUIRED)
	public int addStudent(Student stu) {
		// TODO Auto-generated method stub
		return studentMapper.addStudent(stu);
	}

}
