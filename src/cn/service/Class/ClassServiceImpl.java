package cn.service.Class;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.dao.Class.ClassMapper;
import cn.entity.ClassName;
@Transactional
@Service("ClassService")
public class ClassServiceImpl implements ClassService{
	@Autowired
	private ClassMapper classMapper;

	//查询所有的班级信息
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<ClassName> getClassList() {
		// TODO Auto-generated method stub
		return classMapper.getClassList();
	}

}
