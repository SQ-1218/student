package cn.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;

import cn.entity.ClassName;
import cn.entity.Student;
import cn.service.Class.ClassService;
import cn.service.student.studentService;

@Controller
@RequestMapping("/student")
public class studentController {
	private Logger logger = Logger.getLogger(studentController.class);
	
	@Resource
	private studentService studentService;
	
	@Resource
	private ClassService classService;

	@RequestMapping(value="/index")//��ַ
	public String index(){
		return "index";
	}
	
	@RequestMapping(value="/index", method=RequestMethod.POST)//��ַ
	public String getlist(Model model){
		List<Student> studentList = studentService.getList();		
		model.addAttribute("studentList",studentList);
		return "index";
	}
	
	@RequestMapping(value="/addStudent", method=RequestMethod.GET)//��ַ
	public String add(){
		return "addStudent";
	}
	@RequestMapping(value="/addStudent", method=RequestMethod.POST)//��ַ
	public String addSave(@ModelAttribute("student") Student student,BindingResult bindingResult,HttpSession session){
		if(bindingResult.hasErrors()){
			logger.info("add validated has error================");
			return "addStudent";
		}
		if(studentService.addStudent(student)==1){
			return "redirect:/student/index.html";
		}
		return "addStudent";
	}
	
	@RequestMapping(value="/classlist",method=RequestMethod.POST,produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public Object getclassList(){
		List<ClassName> classList =classService.getClassList();
		logger.info(classList);
		return JSONArray.toJSONString(classList);
	}
}
