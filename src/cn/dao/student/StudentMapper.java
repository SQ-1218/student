package cn.dao.student;

import java.util.List;

import cn.entity.Student;

public interface StudentMapper {
	public List<Student> getList();//查询所有的学员信息
	public int addStudent(Student stu);//增加学员信息 
}
