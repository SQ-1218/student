package cn.entity;

public class Student {
private int  id ;//主键ID
private String name ;//姓名
private int age ;//年龄
private String gender;//性别
private String telephone;//电话
public ClassName getClassName() {
	return className;
}
public void setClassName(ClassName className) {
	this.className = className;
}
private String email ;//邮箱
private int classId ;//班级编号
private ClassName className;


public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getTelephone() {
	return telephone;
}
public void setTelephone(String telephone) {
	this.telephone = telephone;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public int getClassId() {
	return classId;
}
public void setClassId(int classId) {
	this.classId = classId;
}
}
